// Retrieve all the names of the object's properties.
// Return the keys as strings in an array.
// Based on http://underscorejs.org/#keys

const { __esModule } = require("async");

function keys(obj = {}) {
  if (!obj.constructor === Object || !obj) {
    return [];
  }
  let array = [];
  for (let keys in obj) {
    array.push(keys);
  }
  return array;
}

module.exports = keys;
