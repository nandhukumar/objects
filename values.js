// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj = {}) {
  if (!obj.constructor === Object || !obj) {
    return [];
  }
  let array = [];
  for (let keys in obj) {
    if (typeof obj[keys] === "function") {
      continue;
    } else {
      array.push(obj[keys]);
    }
  }
  return array;
}

module.exports = values;
